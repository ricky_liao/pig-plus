-- 支持：需要mysql数据库参数： character_set_server=utf8mb4
-- 本机:localhost 远程采用 % 符号
drop database if exists pig;

drop user if exists 'pig'@'localhost';

create database pig default character set utf8mb4 collate utf8mb4_unicode_ci;

use pig;

create user 'pig'@'localhost' identified by 'pig';

grant all privileges on pig.* to 'pig'@'localhost';

flush privileges;

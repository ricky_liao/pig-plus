-- 提示说明： 新脚本写最底部
-- author:Lucky -- date:20191126--end for: 模板-第三方外链管理 ----------
INSERT INTO `pig`.`sys_menu`(`menu_id`, `name`, `permission`, `path`, `parent_id`, `icon`, `component`, `sort`, `keep_alive`, `type`, `create_time`, `update_time`, `del_flag`) VALUES (170000, '第三方外链管理', NULL, '/link', -1, 'el-icon-odometer', NULL, 5, '1', '0', '2019-11-25 15:45:03', '2019-11-25 15:47:43', '0');
INSERT INTO `pig`.`sys_menu`(`menu_id`, `name`, `permission`, `path`, `parent_id`, `icon`, `component`, `sort`, `keep_alive`, `type`, `create_time`, `update_time`, `del_flag`) VALUES (171000, '阿里云', NULL, '/aliyun', 170000, 'el-icon-odometer', NULL, 1, '1', '0', '2019-11-25 15:45:03', '2019-11-25 15:49:32', '0');
INSERT INTO `pig`.`sys_menu`(`menu_id`, `name`, `permission`, `path`, `parent_id`, `icon`, `component`, `sort`, `keep_alive`, `type`, `create_time`, `update_time`, `del_flag`) VALUES (171001, '阿里云存储', NULL, 'https://www.alibabacloud.com/zh/product/oss', 171000, 'el-icon-odometer', NULL, 5, '1', '0', '2019-11-25 15:45:03', '2019-11-25 15:53:02', '0');
INSERT INTO `pig`.`sys_menu`(`menu_id`, `name`, `permission`, `path`, `parent_id`, `icon`, `component`, `sort`, `keep_alive`, `type`, `create_time`, `update_time`, `del_flag`) VALUES (172000, '腾讯云', NULL, '/tencent', 170000, 'el-icon-odometer', NULL, 2, '1', '0', '2019-11-25 15:45:03', '2019-11-25 15:49:39', '0');
INSERT INTO `pig`.`sys_menu`(`menu_id`, `name`, `permission`, `path`, `parent_id`, `icon`, `component`, `sort`, `keep_alive`, `type`, `create_time`, `update_time`, `del_flag`) VALUES (172001, '腾讯云存储', NULL, 'https://cloud.tencent.com/product/cos', 172000, 'el-icon-odometer', NULL, 1, '1', '0', '2019-11-25 15:45:03', '2019-11-25 15:53:48', '0');
INSERT INTO `pig`.`sys_menu`(`menu_id`, `name`, `permission`, `path`, `parent_id`, `icon`, `component`, `sort`, `keep_alive`, `type`, `create_time`, `update_time`, `del_flag`) VALUES (172002, '腾讯云短信', NULL, 'https://cloud.tencent.com/product/sms', 172000, 'el-icon-odometer', NULL, 2, '1', '0', '2019-11-25 15:45:03', '2019-11-25 15:53:48', '0');
INSERT INTO `pig`.`sys_menu`(`menu_id`, `name`, `permission`, `path`, `parent_id`, `icon`, `component`, `sort`, `keep_alive`, `type`, `create_time`, `update_time`, `del_flag`) VALUES (172003, '微信支付', NULL, 'https://pay.weixin.qq.com/index.php/core/home/login', 172000, 'el-icon-odometer', NULL, 3, '1', '0', '2019-11-25 15:45:03', '2019-11-25 15:53:48', '0');
INSERT INTO `pig`.`sys_menu`(`menu_id`, `name`, `permission`, `path`, `parent_id`, `icon`, `component`, `sort`, `keep_alive`, `type`, `create_time`, `update_time`, `del_flag`) VALUES (172004, '小程序客服', NULL, 'https://mpkf.weixin.qq.com', 172000, 'el-icon-odometer', NULL, 4, '1', '0', '2019-11-25 15:45:03', '2019-11-25 15:53:48', '0');
INSERT INTO `pig`.`sys_menu`(`menu_id`, `name`, `permission`, `path`, `parent_id`, `icon`, `component`, `sort`, `keep_alive`, `type`, `create_time`, `update_time`, `del_flag`) VALUES (173000, '七牛云', NULL, '/qiniuyun', 170000, 'el-icon-odometer', NULL, 3, '1', '0', '2019-11-25 15:45:03', '2019-11-25 15:49:39', '0');
INSERT INTO `pig`.`sys_menu`(`menu_id`, `name`, `permission`, `path`, `parent_id`, `icon`, `component`, `sort`, `keep_alive`, `type`, `create_time`, `update_time`, `del_flag`) VALUES (173001, '七牛云存储', NULL, 'https://www.qiniu.com/products/kodo', 173000, 'el-icon-odometer', NULL, 1, '1', '0', '2019-11-25 15:45:03', '2019-11-25 15:49:39', '0');
-- author:Lucky -- date:20191126--end for: 模板-第三方外链管理 ----------

-- author:Lucky -- date:20191126--end for: 模板-授权角色关联菜单 ----------
INSERT INTO `pig`.`sys_role_menu`(`role_id`, `menu_id`) VALUES (1, 170000);
INSERT INTO `pig`.`sys_role_menu`(`role_id`, `menu_id`) VALUES (1, 171000);
INSERT INTO `pig`.`sys_role_menu`(`role_id`, `menu_id`) VALUES (1, 171001);
INSERT INTO `pig`.`sys_role_menu`(`role_id`, `menu_id`) VALUES (1, 172000);
INSERT INTO `pig`.`sys_role_menu`(`role_id`, `menu_id`) VALUES (1, 172001);
INSERT INTO `pig`.`sys_role_menu`(`role_id`, `menu_id`) VALUES (1, 172002);
INSERT INTO `pig`.`sys_role_menu`(`role_id`, `menu_id`) VALUES (1, 172003);
INSERT INTO `pig`.`sys_role_menu`(`role_id`, `menu_id`) VALUES (1, 172004);
INSERT INTO `pig`.`sys_role_menu`(`role_id`, `menu_id`) VALUES (1, 173000);
INSERT INTO `pig`.`sys_role_menu`(`role_id`, `menu_id`) VALUES (1, 173001);
-- author:Lucky -- date:20191126--end for: 模板-授权角色关联菜单 ----------
